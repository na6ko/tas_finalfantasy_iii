require './job_dugger.rb'

def string_set(ram)
	word_par_line = 5 #<$18.b
	dest = 0x7200 #<$4e.w
	i = 0
	COMMAND.each{|src|
		if ram[(0x7400 & 0xfff) + i] != 0
			break
		end
		i += 1
		ram[0x7ad7 & 0xfff, 0x20] = src[0, 0x20]

		reg_x = 0
		reg_y = 0
		dest += word_par_line
		while reg_x < 0x100 && dest < 0x8000
			t = ram[(0x7ad7 & 0xfff) + reg_x]
			#printf "%02x@%02x ", reg_x, t
			reg_x += 1
			case t
			when 0 #endmark
				#printf("null:%04x\n", 0x7ad7 + reg_x - 1)
				break
			when 1 #改行
				dest += word_par_line * 2
				reg_y = 0
				next
			when 2 #add Y-register
				prev_y = reg_y
				reg_y += ram[(0x7ad7 & 0xfff) + reg_x]
				reg_y &= 0xff
				reg_x += 1
				next
			when 0x29 .. 0x37 #が-ど
				ram[(dest + reg_y - word_par_line) & 0xfff] = 0xc0
				t += 0x66
			when 0x38 .. 0x51
				printf "mijissou %02x\n", t
				exit
			#when 0x38 .. 0x3c #ば-ぼ
			#when 0x3d .. 0x41 #ぱ-ぽ
			#when 0x42 #ヴ
			#when 0x43 .. 0x51 #ガ-ド
			when 0x52 .. 0x56 #バ-ボ
				ram[(dest + reg_y - word_par_line) & 0xfff] = 0xc0
				if t == 0x8a
					t += 0x6b
				end
			when 0x57 .. 0x60 #パ-?
				ram[(dest + reg_y - word_par_line) & 0xfff] = 0xc1
				if t == 0x5a
					t = 0xa6 #へ(ひらがな)
				end
				t += 0x8c
			end
			#printf "%04x=%02x,X:%02x\n", dest + reg_y, t, reg_x - 1
			ram[(dest + reg_y) & 0xfff] = t
			reg_y += 1
			reg_y &= 0xff
		end
		dest += 5 #immediate, it's not <$18.b
	}
end

ITEM_LIST_NUM = 0x20
def string_search(item_list, position_poisona, condition)
	ram_7000 = Array.new(0x1000, 0)
	ram_7000[0x7ad7 & 0xfff, RAM_BASE.size] = RAM_BASE
	ram_7000[0x7b41, 36] = Array.new(36, 1)
		
	(4 + ITEM_LIST_NUM).times{|x|
		ram = ram_7000.dup
		ram[0x7afd & 0xfff, item_list.size] = item_list
		ram[((0x7b41 + 4) & 0xfff) + position_poisona] = 0
		ram[(0x7b3d & 0xfff) + x] = 0
		string_set(ram)
		if 
			ram[0x74ec & 0xfff] != 0 and ram[0x74f3 & 0xfff] == 7 and (x - 4) < position_poisona and (
				(condition == 0 && ram[0x78d3 & 0xfff] != 0) or 
				(condition == 1 && ram[0x75e3 & 0xfff] != 0)
			)
			printf "X:%2d, $75e3:%2d $78d3:%2d\n", x - 4, ram[0x75e3 & 0xfff] & 0xff, ram[0x78d3 & 0xfff] & 0xff
			str = ""
			0.step(item_list.size - 1, 2){|i|
				if item_list[i+1] != 1
					str << sprintf("%2d:0x%02x:%02d ", i / 2, item_list[i] & 0xff, item_list[i+1])
				end
			}
			puts str.chop
		end
	}
end

begin
=begin
---- group #0 pointer enable flag an pointer.h ----
74e9, 2: xx 3
74eb, 2: xx 3
74ed, 1: xx 1
74f3, 3: xx 2 fd
75f1, 2: 3 xx
74f3, 2: 8 xx 1
             0     1     2     3     4     5     6
item-string: xx 03 xx 03 xx 01 xx 02 fd 03 xx 08 xx 01

---- group #1 end battle flag ----
78d1, 2: xx 3
78d3, 1: xx 
#78d1, 78d2, 78d3
=end
	item_list_plain = []
	ITEM_LIST_NUM.times{|i|
		item_list_plain << (0x100 | i)
		item_list_plain << 1
	}
	(ITEM_LIST_NUM - 6).times{|item_group_pointer|
		item_list_withpointer = item_list_plain.dup
		i = item_group_pointer * 2
		i += 1
		item_list_withpointer[i] = 3
		i += 2
		item_list_withpointer[i] = 3
		i += 4
		item_list_withpointer[i] = 2
		i += 1
		item_list_withpointer[i] = 0xfd
		i += 1
		item_list_withpointer[i] = 3
		i += 2
		item_list_withpointer[i] = 7
		
		32.times{|item_group_endbattle|
			item_list = item_list_withpointer.dup
			i = item_group_endbattle * 2
			i += 1
			item_list[i] = 3
			string_search item_list, item_group_pointer+4, 0
		}
	}

	(ITEM_LIST_NUM - 6).times{|item_group_pointer|
		item_list_withpointer = item_list_plain.dup
		i = item_group_pointer * 2
		i += 1
		item_list_withpointer[i] = 3
		i += 2
		item_list_withpointer[i] = 3
		i += 4
		item_list_withpointer[i] = 2
		i += 1
		item_list_withpointer[i] = 0xfd
		i += 1
		item_list_withpointer[i] = 3
		i += 2
		item_list_withpointer[i] = 7
		string_search item_list_withpointer, item_group_pointer+4, 1
	}
end
