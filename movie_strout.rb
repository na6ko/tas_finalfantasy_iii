class String
	def mb_ljust(width, padding=' ')
		output_width = each_char.map{|c| c.bytesize == 1 ? 1 : 2}.reduce(0, &:+)
		padding_size = [0, width - output_width].max
		self + padding * padding_size
	end
end

def str_arrange(s, w)
	line_offset = 0
	t = ""
	s.split(//).each{|c|
		if line_offset == 0 && t != "" && c =~ /[\.,]/
			t = t[0, t.size - 2]
			t << c << '\n'
			next
		elsif line_offset == 0 && t != "" && c == ' '
			next
		end
		line_offset += c.chr.bytesize == 1 ? 1 : 2
		t << c
		if line_offset >= w
			t << '\n'
			line_offset = 0
		end
	}
	t.gsub(/([―ソ噂浬欺圭構蚕十申曾箪貼能表暴予禄])/, '\1\\').encode(Encoding::CP932)
end
def explanation(src, dest, str_width)
	ar = []
	str = ""
	i = 0
	frame_prev = nil
	File.open(src).each{|t|
		m = /@(\d+)\n/.match(t)
		if m != nil
			frame = m[1].to_i
			if i != 0
				delta = (frame - frame_prev) / (i + 1)
				#p i,frame_prev, frame, delta
				i.times{|j|
					frame_prev += delta
					str.sub!(sprintf("####%d####\n", j), sprintf("@%d\n", frame_prev))
				}
				i = 0
			end
			frame_prev = frame
		elsif t == "%\n"
			t = sprintf("####%d####\n", i)
			i += 1
		end
		str << t
	}
	str.split(/\n?@/).each{|t|
		s = t.split("\n")
		if s[0] == nil
			next
		end
		frame = s.shift.to_i
		ar << {:frame => frame, :str => s}
	}

	f = File.open(dest, 'w')
	f.puts "function explanation()\nlocal str = \"\""
	ar.size.times{|i|
		if ar[i][:str].size == 0
			next
		end
		str = ''
		if (i + 1) < ar.size
			str = sprintf("if obj.frame >= %d and obj.frame < %d then\n", ar[i][:frame] + 16, ar[i+1][:frame])
		else
			str << sprintf("if obj.frame >= %d then", ar[i][:frame])
		end
		str << "\n" << 'str = "'
		ar[i][:str].each{|t|
			if t =~ /<#[0-9a-f]{6}>/
				str << t
				next
			end
			str << str_arrange(t, str_width) << "\\n"
		}
		str << '"'
		f.puts str
		f.puts "end"
	}
	f.puts "return str\nend"
	
	f.puts "function str_timer()\nlocal v = 0"
	ar.size.times{|i|
		str = ''
		if ar[i][:str].size == 0
			next
		end
		if (i + 1) < ar.size
			str << sprintf("if obj.frame >= %d and obj.frame < %d then", ar[i][:frame], ar[i+1][:frame])
		else
			break
		end
		str << "\n" << sprintf("v = (%d - obj.frame)/(%d - %d)", ar[i+1][:frame], ar[i+1][:frame], ar[i][:frame])
		f.puts str
		f.puts "end"
	}
	f.printf("sector(360*v, %d, %d)\n", 620, -340)
	f.puts "end"
	f.close
end

def lua_function_put(f, d, function_name, callback)
	f.puts "function " + function_name + "()"
	f.puts 'local str = ""'
	d.size.times{|i|
		str = ''
		if i == 0
			str = sprintf("if obj.frame < %d then\n", d[i][:timestamp])
		end
		if (i + 1) < d.size
			str << sprintf("elseif obj.frame < %d then", d[i+1][:timestamp])
		else
			str = "else"
		end
		str << "\n"
		str << callback.call(d[i])
		f.puts str
	}
	f.puts "end\nreturn str\nend"
end
def color_set(val)
	val == 0 ? 0x909090 : 0xffffff
end
def ramputs(src, dest)
	d = {:input_buffer => [], :script_enable => [], :script_pointer => [], :battle_end => [], :item => []}
	File.open(src).each{|t|
		t = t.strip.split(/\s+/)
		s = {:timestamp => t.shift.to_i}
		case t.shift
		when "@0021"
			s[:data] = t.shift.to_i(0x10)
			d[:input_buffer] << s
		when "@006c"
			s[:data] = t.shift.to_i(0x10)
			d[:script_enable] << s
		when "@0072"
			s[:addr] = t.shift.to_i(0x10)
			s[:data] = t.shift.to_i(0x10)
			d[:script_pointer] << s
		when "@60c0"
			s[:data] = t.map{|ss|
				ss.to_i(0x10)
			}
			d[:item] << s
		when "@78d3"
			s[:data] = t.shift.to_i(0x10)
			d[:battle_end] << s
		else
			puts "unknown @"
			exit
		end
	}
	f = File.open(dest, 'w')
	callback = Proc.new{|d|
		sprintf("str = \"<#%06x>%02X\"\n", color_set(d[:data]), d[:data])
	}
	lua_function_put(f, d[:input_buffer], "memory_input_buffer", callback)

	lua_function_put(f, d[:battle_end], "memory_battle_end", callback)

	lua_function_put(f, d[:script_enable], "memory_script_enable", callback)

	callback = Proc.new{|d|
		sprintf("str = \"%04X@%02X\"\n", d[:addr], d[:data])
	}
	lua_function_put(f, d[:script_pointer], "memory_script_pointer", callback)
	
	callback = Proc.new{|d|
		i = 0
		str = ""
		color_prev = nil
		d[:data].each{|t|
			color = color_set(t)
			if color != color_prev
				str << sprintf("<#%06x>", color)
			end
			color_prev = color
			str << sprintf("%02X", t)
			if i == d[:data].size - 1
				break
			end
			if (i & 0x0f) == 0x0f
				str << '\n'
			elsif (i & 1) == 1
				str << '<p+8,+0>'
			end
			i += 1
			if i >= 0x10
				i = 0
			end
		}
		'str="' + str.chomp + '"'
	}
	lua_function_put(f, d[:item], "memory_item", callback)
	f.close
end
begin
	if ARGV.size == 0
		puts "explanation [src] [dest]"
		puts "expw [src] [dest] [width]"
		puts "ramputs [src] [dest]"
		exit
	end
	case ARGV.shift
	when "explanation"
		explanation ARGV[0], ARGV[1], 26
	when "expw"
		explanation ARGV[0], ARGV[1], ARGV[2].to_i
	when "ramputs"
		ramputs ARGV[0], ARGV[1]
	else
		puts 'unknown command'
	end
end
