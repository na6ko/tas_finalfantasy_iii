local battle_sum
function battle_sum_get()
	battle_sum = memory.readbyte(0x0015)
	print(string.format("sum:0x%02x", battle_sum))
end
function battle_sum_set()
	print(string.format("sum=0x%02x", battle_sum))
	memory.setregister("a", battle_sum)
end

--memory.registerexecute(0xfc30, 1, battle_sum_get)
memory.registerexecute(0xfc2e, 1, battle_sum_set)
battle_sum = 0x44
