local w0072 = 0
local script_data = 0
function script_pointer_written()
	w0072 = 1
	if memory.readbyte(0xa5fe) ~= 0xf8 then
		do return end
	end
	script_data = memory.readbyte(memory.readwordunsigned(0x0072))
end
local w006c = 0
function script_enable_written()
	w006c = 1
end
local endflag = 0
function battleend_written()
	endflag = 1
end
local itemflag = 0
function item_slots_written()
	itemflag = 1
end
local w0021 = 0
function input_buffer_written()
	w0021 = 1
end

memory.registerwrite(0x0021, 1, input_buffer_written)
memory.registerwrite(0x7480+0x0021, 1, input_buffer_written)
memory.registerwrite(0x0073, 1, script_pointer_written)
memory.registerwrite(0x74f2, 2, script_pointer_written)
memory.registerwrite(0x006c, 1, script_enable_written)
memory.registerwrite(0x74ec, 1, script_enable_written)
memory.registerwrite(0x78d3, 1, battleend_written)
memory.registerwrite(0x60c0, 0x40, item_slots_written)

function zp_switch(zp, wram)
	if memory.readwordunsigned(0x0101) == 0xff2a then
		do return zp end
	end
	return wram
end
local script_addr_prev = -1
local script_enable_prev = -1
local input_buffer_prev = 0
while true do
	if w0021 ~= 0 then
		w0021 = 0
		local data_zp = memory.readbyte(0x0021)
		local data_wram = memory.readbyte(0x7480 + 0x0021)
		local d = zp_switch(data_zp, data_wram)
		if input_buffer_prev ~= d then
			print(string.format("%6d @0021 %02x", emu.framecount(), d))
		end
		input_buffer_prev = d
	end
	if w006c ~= 0 then
		w006c = 0
		local data_zp = memory.readbyte(0x006c)
		local data_wram = memory.readbyte(0x74ec)
		local data_sum = data_zp + data_wram
		if script_enable_prev ~= data then
			print(string.format("%6d @006c %02x", emu.framecount(), zp_switch(data_zp, data_wram)))
		end
		script_enable_prev = data_sum
	end
	if w0072 ~= 0 then
		w0072 = 0
		local addr_zp = memory.readwordunsigned(0x0072)
		local addr_wram = memory.readwordunsigned(0x74f2)
		local addr_sum = addr_zp + addr_wram
		if script_addr_prev ~= addr_sum then
			print(string.format("%6d @0072 %04x %02x", emu.framecount(), zp_switch(addr_zp, addr_wram), script_data))
		end
		script_addr_prev = addr_sum
	end
	if endflag ~= 0 then
		endflag = 0
		print(string.format("%6d @78d3 %02x", emu.framecount(), memory.readbyte(0x78d3)))
	end
	if itemflag ~= 0 then
		itemflag = 0
		local str = string.format("%6d @60c0", emu.framecount())
		for a = 0x60c0, 0x60ff do
			str = str .. string.format(" %02x", memory.readbyte(a))
		end
		print(str)
	end
	emu.frameadvance()
end
