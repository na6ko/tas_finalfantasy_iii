require 'win32/clipboard'
def tasvideos(filename)
	str = ''
	url_prefix = ''
	visible = true
	File.open(filename).each{|t|
		if t.include?("[module") == false
			t.gsub!('[', '[[')
			t.gsub!(']', ']]')
		end
		
		if t =~ /\A\* /
			if t.include?(' @') == false
				visible = true
			elsif t.include?(' @T')
				visible = true
				t.sub!(/ @.+\n/, "\n")
				p t
			else
				visible = false
			end
		end
		if t =~ /\A\*+/
			t.sub!('***', '!')
			t.sub!('**', '!!')
			t.sub!('*', '!!!')
			t.sub!(/ \[.+\]\n/, "\n") #referernce by pukiwiki 
		elsif t =~ /\A\-+/
			t.sub!(/\A\-{3} /, '*** ')
			t.sub!(/\A\-{2} /, '** ')
			t.sub!(/\A\-{1} /, '* ')
		elsif t =~ /\A\+/
			t.sub!('+', '#')
		elsif t =~ /\A\t/
			t = ' ' + t
		elsif t =~ /\A#ref_prefix/ #config; no description
			url_prefix = t.split(/\s/)[1]
			next
		elsif t =~ /\A#ref\(/
			m = /\(([^\)]+)\)/.match(t)
			t = '[' + url_prefix + m[1] + ']' + "\n"
		end
		if visible == true
			str += t
		end
	}
	Win32::Clipboard.set_data(str, Win32::Clipboard::UNICODETEXT)
end

def hatenablog(filename)
	str = ''
	pre = 0
	paragraph = 0
	visible = Array.new(4, true)
	level = 0
	File.open(filename).each{|t|
		if t =~ /\A\*+/
			if t =~ /\*\*\*+/
				paragraph = 3
			elsif t =~ /\*\*/
				paragraph = 2
			else
				paragraph = 1
			end
			v = true
			if t.include? '@'
				cc = t.chomp.split('@')
				v = cc[1] == 'H'
				t = cc[0] + "\n"
			end
			if visible[paragraph - 1] == true
				(paragraph..(visible.size - 1)).each{|i|
					visible[i] = v
				}
			end
			#p t,v,paragraph, visible
		elsif t =~ /\A[^ \t]/
			case pre
			when 1
				#t = "||<\n" + t
				t = "@@!!@@\n" + t
			end
			pre = 0
		elsif t =~ /\A[ \t]/
			#p t
			case pre
			when 0
				t = ">||\n" + t
				pre = 1
			end
		elsif t =~ /\A#ref_prefix/
			next
		end
		
		if pre == 0
			t.gsub!('>', '&gt;')
			t.gsub!('<', '&lt;')
			t.gsub!('@@!!@@', '||<')
		end
		
		if visible[paragraph]
			str += t
		end
	}
	Win32::Clipboard.set_data(str, Win32::Clipboard::UNICODETEXT)
end
begin
	case ARGV.shift
	when 'tasvideos'
		tasvideos(ARGV.shift)
	when 'hatenablog'
		hatenablog(ARGV.shift)
	else
		puts "[tasvideos|hatenablog] [src pukiwiki text file]"
		exit
	end
end
